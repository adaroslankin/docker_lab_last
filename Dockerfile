FROM python:3.7-slim-buster

WORKDIR /usr/src/app

COPY /src/* ./
COPY /requirements.txt ./

RUN python3 -m pip install -r requirements.txt

CMD [ "python3", "./app.py" ]